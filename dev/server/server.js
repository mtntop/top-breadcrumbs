require('dotenv').config();
const express = require('express');
const app = express();
const path = require('path');

app.use(express.static(path.resolve(__dirname, './')));

const server = app.listen(process.env.PORT || 3002, () => {
  const host = server.address().address;
  const port = server.address().port;
  console.log('MPowerTable http://%s:%s', host, port);
});
