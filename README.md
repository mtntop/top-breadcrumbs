# Top Dashboard

## Usage example

![Usage example](http://g.recordit.co/n7HPVK0swF.gif 'Usage example')

In `main.js`:

```javascript
import TopBreadcrumbs from '@mtntop/breadcrumbs';

Vue.use(TopBreadcrumbs);
```

Template:

```vue
<template>
  <div class="container pt-3">
    <breadcrumb
      :items="{
        Organizations: { path: `/organizations` },
        Organization: {
          path: `/organizations/${$route.params.organization_id}`,
        },
        Users: {
          path: `/organizations/${$route.params.organization_id}/users`,
        },
        'John Doe': true,
      }"
    />
  </div>
</template>

<script>
export default {
  name: 'App',
  data() {
    return {};
  },
};
</script>
```
