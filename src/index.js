import Vue from 'vue';
import TopBreadcrumbs from './components/TopBreadcrumbs';
const VERSION = require('../package.json').version;

export default {
  install(Vue, options) {

    Vue.component('breadcrumbs', TopBreadcrumbs);
    console.log('MPowerTopBreadcrumbs %s', VERSION);
  }
};
